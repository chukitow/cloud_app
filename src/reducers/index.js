import { combineReducers } from 'redux';
import LoginReducer from './login';
import DropsReducer from './drops';
import DropsPaginationReducer from './drops/pagination';

const rootReducer = combineReducers({
  login: LoginReducer,
  drops: DropsReducer,
  dropsPagination: DropsPaginationReducer,
});

export default rootReducer;
