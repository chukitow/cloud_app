import _ from 'lodash'
import  { FETCH_DROPS } from '../../actions/drops';

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_DROPS:
      return _.uniqBy([...state, ...action.payload], 'slug');
      break;
    default:
      return [...state];
  }
}
