import  { UPDATE_PAGINATION } from '../../actions/drops';

export default function(state = {}, action) {
  switch (action.type) {
    case UPDATE_PAGINATION:
      return {...action.payload};
      break;
    default:
      return {...state};
  }
}
