import {
  SET_EMAIL,
  SET_PASSWORD,
  SUBMIT_LOGIN,
  LOGIN_FAIL,
  LOGIN_SUCCESS
} from '../../actions/login';

const INITIAL_STATE = {
  email: '',
  passowrd: '',
  showLoader: false,
  showAlert: false,
  user: null,
};

export default function(state = INITIAL_STATE, action){
  switch (action.type) {
    case SET_EMAIL:
      return {...state, email: action.payload, showAlert: false};
    case SET_PASSWORD:
      return {...state, password: action.payload, showAlert: false};
    case SUBMIT_LOGIN:
      return { ...state, showLoader: true, showAlert: false};
    case LOGIN_FAIL:
      return { ...state, showLoader: false, showAlert: true };
    case LOGIN_SUCCESS:
      return { ...state, showLoader: false, showAlert: false};
    default:
      return { ...state };
  }
}
