import axios from 'axios';
import { AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';

export const SET_EMAIL      = 'SET_EMAIL';
export const SET_PASSWORD   = 'SET_PASSWORD';
export const SUBMIT_LOGIN   = 'SUBMIT_LOGIN';
export const LOGIN_SUCCESS  = 'LOGIN_SUCCESS';
export const LOGIN_FAIL     = 'LOGIN_FAIL';

export function setEmail(email) {
  return {
    type: SET_EMAIL,
    payload: email,
  };
}

export function setPassword(password) {
  return {
    type: SET_PASSWORD,
    payload: password,
  };
}

export function submitLogin(user, password){
  return function(dispatch) {
    dispatch({type: SUBMIT_LOGIN});
    axios.get('https://cloudapp-api.herokuapp.com/items', {
      headers: {
        user,
        password
      }
    })
    .then(res => {
      dispatch({type: LOGIN_SUCCESS, payload: res.data})
      AsyncStorage.setItem('user', JSON.stringify({ user, password }));
      Actions.dashboard({type: "reset"});
    })
    .catch(res => dispatch({type: LOGIN_FAIL}))
  }
}
