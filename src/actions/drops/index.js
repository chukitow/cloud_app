import axios from 'axios';
import { AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';

export const FETCH_DROPS = 'FETCH_DROPS';
export const UPDATE_PAGINATION = 'UPDATE_PAGINATION';

export function fetchDrops(query = '') {
  return function(dispatch) {
    console.log(`https://cloudapp-api.herokuapp.com/items?${query}`);
    AsyncStorage
      .getItem('user')
      .then((data) => JSON.parse(data))
      .then(({user, password}) => axios.get(`https://cloudapp-api.herokuapp.com/items?${query}`, { headers: {user, password} }))
      .then((response) => {
        dispatch({type: FETCH_DROPS, payload: response.data.data });
        dispatch({type: UPDATE_PAGINATION, payload: response.data.links });
      })
  }
}
