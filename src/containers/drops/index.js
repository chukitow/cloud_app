import React, {  Component } from 'react';
import { View, ListView, StatusBar, StyleSheet, Text } from 'react-native';
import GridView from 'react-native-grid-view';
import { connect } from 'react-redux';
import { fetchDrops } from '../../actions/drops';
import DropItem from '../../components/drops/item';

class Drops extends Component {
  constructor(props){
    super(props);
    StatusBar.setBarStyle('light-content', true);
  }
  componentWillMount() {
    this.props.fetchDrops();
  }

  renderItem(drop) {
    return(
      <DropItem key={drop.slug} drop={drop} />
    );
  }

  loadOtherPage() {
    if(this.props.pagination.next_url){
      this.props.fetchDrops(`next_page=${this.props.pagination.next_url.href}`);
    }
  }

  render() {
    return(
      <GridView
       items={this.props.dataSource}
       itemsPerRow={2}
       enableEmptySections={true}
       onEndReached={() => this.loadOtherPage()}
       onEndReachedThreshold={400}
       renderItem={this.renderItem} />
    );
  }
}

function mapStateToProps(state) {
  return { dataSource: state.drops, pagination: state.dropsPagination};
};

const styles = StyleSheet.create({
  list: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
});

export default connect(mapStateToProps, { fetchDrops })(Drops);
