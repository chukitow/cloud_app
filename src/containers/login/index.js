import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setEmail, setPassword, submitLogin } from '../../actions/login';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  Alert,
} from 'react-native';

class Login extends Component {
  componentWillReceiveProps (nextProps) {
    this.showAlert(nextProps.showAlert);
  }

  showAlert(show) {
    if(show){
      Alert.alert('Invalid email or password');
    }
  }

  showSpinnerOrTtext() {
    if(!this.props.showLoader) {
      return(
        <Text style={styles.loginText}>Log in</Text>
      );
    }

    return(
      <ActivityIndicator
        animating={true}
        style={{height: 20}}
        size="small"
      />
    );
  }

  render() {
    return(
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          keyboardType="email-address"
          autoCapitalize="none"
          value={this.props.email}
          onChangeText={(val) => this.props.setEmail(val)} />

        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          value={this.props.passowrd}
          onChangeText={(val) => this.props.setPassword(val)} />

        <TouchableOpacity  activeOpacity={0.8} onPress={() => this.props.submitLogin(this.props.email, this.props.password)} style={styles.button}>
          <View>
            {this.showSpinnerOrTtext()}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15,
  },
  img: {
    marginBottom: 10,
  },
  input: {
    alignSelf: 'stretch',
    height: 50,
    borderColor: "rgba(0,0,0,0.07)",
    padding: 10,
    borderWidth: 1,
  },
  button: {
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    height: 50,
    backgroundColor: "#286090",
    borderColor: "#204d74",
  },
  loginText: {
    color: "#fff",
    fontSize: 18,
  }
});

function mapStateToProps(state){
  const { email, password, showLoader, showAlert } = state.login;

  return { email, password, showLoader, showAlert }
}

export default connect(mapStateToProps, { setEmail, setPassword, submitLogin})(Login);
