import React from 'react';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import {Scene, Router} from 'react-native-router-flux';
import rootReducer from './reducers';
import Placeholder from './components/placeholder';
import Login from './containers/login';
import Drops from './containers/drops';

const store = applyMiddleware(ReduxThunk)(createStore);

const App = () => {
  return(
    <Provider store={store(rootReducer)}>
      <Router>
        <Scene key="root">
          <Scene key="Placeholder" component={Placeholder} hideNavBar={true} />
        </Scene>
        
        <Scene key="auth">
          <Scene key="login" component={Login} hideNavBar={true} />
        </Scene>

        <Scene key="dashboard">
          <Scene key="items"  navigationBarStyle={styles.navbar} sceneStyle={styles.sceneStyle} titleStyle={styles.title} component={Drops} title="All Drops" />
        </Scene>
      </Router>
    </Provider>
  );
}

const styles = {
  sceneStyle: {
    paddingTop: 65,
  },
  navbar: {
    backgroundColor: "#2b303b",
  },
  title: {
    color: "#fff",
  }
};

export default App;
