import React, { Component } from 'React'
import { View } from 'react-native'

const Item = ({ children }) => {
  return(
    <View style={styles.containerStyle}>
      {children}
    </View>
  );
}

const styles = {
  containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative'
  }
}

export default Item;
