import React, { Component } from 'react'
import { View } from 'react-native'
import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';
import axios from 'axios';

class Placeholder extends Component {
  componentWillMount(){
    AsyncStorage.getItem('user')
   .then((data) => {
     if(data) {
        const user = JSON.parse(data);
        return axios.get('https://cloudapp-api.herokuapp.com/items', { headers: { user: user.user, password: user.password } })
        .then(() => Actions.dashboard({ type: "reset" }))
     }
     else {
       Actions.auth({ type: "reset" });
     }
   })
   .catch(() => Actions.auth({ type: "reset" }));

  }

  render() {
    return(
      <View>
      </View>
    );
  }
}

export default Placeholder;
