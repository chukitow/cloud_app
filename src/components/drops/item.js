import React from 'react';
import { View, Text, Image } from 'react-native';
import Card from '../../components/common/card';
import CardItem from '../../components/common/card/item';

const DropItem = ({ drop }) => {
  return(
    <Card>
      <CardItem>
        <Image
          source={ {uri: drop.thumbnail_url } }
          style={styles.cover} />
      </CardItem>
      <CardItem>
        <Text>
          {drop.name.substr(0, 20)}...
        </Text>
      </CardItem>
    </Card>
  );
}

const styles = {
  cover: {
    height: 100,
    width: 150,
  },
}

export default DropItem
